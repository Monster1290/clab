//
//  main.cpp
//  Clab
//
//  Created by Александр Беседин on 01/12/2018.
//  Copyright © 2018 Александр Беседин. All rights reserved.
//

#include <iostream>
#include <string>
#include "stack.hpp"
#include <fstream>

using namespace std;

bool isClosingBracket(char sym) {
    return sym == ')' || sym == ']' || sym == '}';
}

bool isPairedBrackets(char open, char closing) {
        return
            (open == '(' && closing == ')') ||
            (open == '[' && closing == ']') ||
            (open == '{' && closing == '}');
}

bool checkBracketBalance(string str) {
    stack bracketsStack = stack();
    stack closingBracketsStack = stack();
    const char *strData = str.data();
    const int strSize = str.size();
    for (int i = 0; i < strSize; i++) {
        char sym = strData[i];
        bool isBracket =
            sym == '(' || sym == ')' ||
            sym == '[' || sym == ']' ||
            sym == '{' || sym == '}';
        
        if (!isBracket) continue;
        bracketsStack.push(strData[i]);
    }
    
    if (bracketsStack.isEmpty()) return false;
    
     do {
         char bracket = bracketsStack.pop();
         
         if (isClosingBracket(bracket)) {
             closingBracketsStack.push(bracket);
         } else if (!closingBracketsStack.isEmpty()) {
             
             char openBracket = bracket, closingBracket = closingBracketsStack.pop();
             if (!isPairedBrackets(openBracket, closingBracket)) return false;
                 
         } else {
             return false;
         }
     } while (!bracketsStack.isEmpty());
    
    if (!closingBracketsStack.isEmpty()) return false;
    
    return true;
}

int main(int argc, const char * argv[]) {
    if (argc)
    
    if (argc == 1) {
        printf("\nNAME\n    BSC - brackets sequence cheker\nSYNOPSIS\n    ./bsc [file_path]\nDESCRIPTION\n    For input file checks correct or not brackets sequence.\n");
        return 1;
    }
    
    if (argc == 3) {
        printf("Incorrect count of input parameters. Run programm without paremetrs to see how it use.\n");
        return 1;
    }
    
    ifstream file(argv[1]);
    
    if (!file.is_open()) {
        printf("File at given file path doesn't exsist. Please, check file path.\n");
        return 1;
    }
    
    string line;
    
    getline(file, line);
    
    if (checkBracketBalance(line)) {
        cout << "Correct brackets sequence" << endl;
    } else {
        cout << "Wrong brackets sequence" << endl;
    }
    return 0;
}
