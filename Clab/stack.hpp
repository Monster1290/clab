//
//  stack.hpp
//  Clab
//
//  Created by Александр Беседин on 01/12/2018.
//  Copyright © 2018 Александр Беседин. All rights reserved.
//

#ifndef stack_hpp
#define stack_hpp

#include <stdio.h>
#include <vector>
class stack {
    const short EMPTY_INDEX = -1;
    int topIndex = EMPTY_INDEX;
    std::vector<char> stack = std::vector<char>();
    
public:
    char top();
    void push(char value);
    char pop();
    bool isEmpty();
    int size();
};

#endif /* stack_hpp */
