//
//  stack.cpp
//  Clab
//
//  Created by Александр Беседин on 01/12/2018.
//  Copyright © 2018 Александр Беседин. All rights reserved.
//

#include "stack.hpp"

char stack::top() {
    return stack[topIndex];
}

void stack::push(char value) {
    if (stack.size() == stack.capacity())
        stack.resize(stack.capacity() + 10);
    
    stack[++topIndex] = value;
}

char stack::pop() {
    return stack[topIndex--];
}

bool stack::isEmpty() {
    return topIndex == EMPTY_INDEX;
}

int stack::size() {
    return topIndex + 1;
}

